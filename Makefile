#!make

BUILD_CACHE_PREFIX="ideaplexus/"
PROXY_CACHE_PREFIX=""

.PHONY: lint-alpine
lint-alpine:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine < Dockerfile.alpine

.PHONY: lint-archlinux
lint-archlinux:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine < Dockerfile.archlinux

.PHONY: build-alpine
build-alpine:
	docker build --file Dockerfile.alpine --tag $(BUILD_CACHE_PREFIX)samba-ad-dc:alpine --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

.PHONY: build-archlinux
build-archlinux:
	docker build --file Dockerfile.archlinux--tag $(BUILD_CACHE_PREFIX)samba-ad-dc:archlinux --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

.PHONY: all
all: lint-alpine lint-archlinux build-alpine build-archlinux